
import Foundation
import UIKit

extension UIView {
  
  var firstResponder: UIView? {
    guard !isFirstResponder else {
      return self
    }
    for view in subviews {
      guard let responder = view.firstResponder else {
        continue
      }
      return responder
    }
    return nil
  }
  
}
