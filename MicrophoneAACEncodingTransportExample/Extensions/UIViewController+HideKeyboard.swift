
import Foundation
import UIKit

extension UIViewController {
  
  @objc func hideKeyboard() {
    UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
  }
  
}
