//
//  ViewController.swift
//  MicrophoneAACEncodingTransportExample
//
//  Created by Shane Whitehead on 31/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

	@IBOutlet weak var portTextField: UITextField!
	@IBOutlet weak var ipAddressTextField: UITextField!
	@IBOutlet weak var captureButton: UIButton!
	
	var service: CaptureAudioService?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		ipAddressTextField.text = UserDefaults.standard.string(forKey: "IPAddress")
		portTextField.text = UserDefaults.standard.string(forKey: "port") ?? "8686"
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		let session = AVAudioSession.sharedInstance()
		session.requestRecordPermission { (granted) in
			guard granted else {
				print("Microphone permission has been denied")
				return
			}
			do {
				try session.setCategory(.playAndRecord,
																mode: .measurement,
																options: [.interruptSpokenAudioAndMixWithOthers,
																					.allowBluetooth,
																					.allowBluetoothA2DP,
																					.allowAirPlay,
																					.defaultToSpeaker])
				try session.setCategory(.playAndRecord)
				try session.setActive(true, options: [])
				
				print(">> session.isInputGainSettable = \(session.isInputGainSettable)")
				try session.setInputGain(1.0)
			} catch let error {
				print("!! Could not set session catagory: \(error.localizedDescription)")
			}
		}
	}

	@IBAction func capture(_ sender: Any) {
		if let service = service {
			service.stop()
			self.service = nil
			captureButton.setTitle("Capture", for: [])
			return
		}
		
		guard let ipAddress = ipAddressTextField.text else {
			return
		}
		guard let value = portTextField.text, let port = UInt16(value) else {
			return
		}
		
		UserDefaults.standard.set(ipAddress, forKey: "IPAddress")
		UserDefaults.standard.set(port, forKey: "port")

		do {
			let config = DeviceConfiguration(ipAddress: ipAddress, port: port)
			service = CaptureAudioService()
			try service?.start(config: config)
			
			// If you enable this, don't forget to disable the microphone service in the CaptureAudioService
//			DispatchQueue.global(qos: .userInitiated).async {
//				TransportQueue.shared.put(Bundle.main.url(forResource: "TestAudioCapture", withExtension: "aac")!)
//			}
			
			captureButton.setTitle("Stop", for: [])
		} catch let error {
			print("!! Failed to spin up capture audio service: \(error.localizedDescription)")
			service = nil
		}
	}
	
}

