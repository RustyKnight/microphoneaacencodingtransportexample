//
//  AudioTapFileCache.swift
//  MicrophoneAACEncodingTransportExample
//
//  Created by Shane Whitehead on 21/9/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import AVFoundation

//class AudioSnippet {
//	let url: URL
//	var audioFile: AVAudioFile?
//
//	init(url: URL, audioFile: AVAudioFile) {
//		self.url = url
//		self.audioFile = audioFile
//	}
//}

class FileCacheAudioTap: AudioTap {
	
	fileprivate var recording: AVAudioFile?
	var masterAudioFile: AVAudioFile?
	var count = 0
	
	init(audioFile: AVAudioFile) {
		self.masterAudioFile = audioFile
	}
	
	func open() {
//		clearCache()
		recording = makePublicAudioFile(named: "TestAudioCapture")
	}
	
	func close() {
		recording = nil
		masterAudioFile = nil
	}
	
	func drip(buffer: AVAudioPCMBuffer, time: AVAudioTime) {
		print("...drip")
		do {
			// This is the "master" audio file we're using
			// as a buffer
			if let audioFile = masterAudioFile {
				try audioFile.write(from: buffer)
			}
			// This is for test ;)
			if let recording = self.recording {
				try recording.write(from: buffer)
			}
//			count += 1
//			let num = String(format: "%04d", count)
//			guard let snippet = self.makeAudioFile(named: "Snippet-\(num)") else { return }
//			if let audioFile = snippet.audioFile {
//				try audioFile.write(from: buffer)
//			} else {
//				print("!! Missing audio snippet")
//			}
//
//			FileQueue.shared.put(snippet)
		} catch let error {
			print("!! \(error)")
		}
	}
	
	fileprivate func makePublicAudioFile(named: String) -> AVAudioFile? {
		var url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
		url.appendPathComponent("\(named).aac", isDirectory: false)
		
		let settings: [String: Any] = [
			AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),
			AVSampleRateKey: NSNumber(value: 8000),
			AVNumberOfChannelsKey: NSNumber(value: 1),
			AVEncoderBitRatePerChannelKey: NSNumber(value: 16),
			AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.min.rawValue)
		]
		do {
			let audioFile = try AVAudioFile(forWriting: url, settings: settings)
			return audioFile
		} catch let error {
			print("!! Failed to create public audio capture file: \(error)")
		}
		return nil
	}
//
//	fileprivate func makeAudioFile(named: String = UUID().uuidString) -> AudioSnippet? {
//		var url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
//		url.appendPathComponent("\(named).aac", isDirectory: false)
//
//		let settings: [String: Any] = [
//			AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),
//			AVSampleRateKey: NSNumber(value: 8000),
//			AVNumberOfChannelsKey: NSNumber(value: 1),
//			AVEncoderBitRatePerChannelKey: NSNumber(value: 16),
//			AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.min.rawValue)
//		]
//		do {
//			let audioFile = try AVAudioFile(forWriting: url, settings: settings)
//			return AudioSnippet(url: url, audioFile: audioFile)
//		} catch let error {
//			print("!! Failed to create audio snippet file: \(error)")
//		}
//		return nil
//	}
//
//	fileprivate func clearCache() {
//		do {
//			let url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
//			let contents = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: [.skipsSubdirectoryDescendants])
//			for child in contents {
//				let name = child.lastPathComponent
//				guard !(name.hasPrefix("Test") && name.hasSuffix(".aac")) && child.isFileURL else { continue }
//				print("Remove \(name)")
//				do {
//					try FileManager.default.removeItem(at: child)
//				} catch let error {
//					//					print("!! Could not remove")
//					//					print("!! \(child)")
//					//					print("!! \(error)")
//				}
//			}
//		} catch let error {
//			print("!! Clean failed: \(error)")
//		}
//	}
	
}
