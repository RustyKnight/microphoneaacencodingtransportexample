//
//  AudioTap.swift
//  MicrophoneAACEncodingTransportExample
//
//  Created by Shane Whitehead on 21/9/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import AVFoundation

protocol AudioTap {
	func open()
	func close()
	func drip(buffer: AVAudioPCMBuffer, time: AVAudioTime)
}
