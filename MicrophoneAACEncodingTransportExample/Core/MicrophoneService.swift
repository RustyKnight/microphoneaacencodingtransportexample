//
//  MicrophoneService.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import AVFoundation

// Base on https://stackoverflow.com/questions/39595444/avaudioengine-downsample-issue
// https://github.com/onmyway133/notes/issues/367

class MicrophoneService {
	
//	let micMixerNode: AVAudioMixerNode = AVAudioMixerNode()
	
	fileprivate var audioFile: AVAudioFile?
	
	init(audioFile: AVAudioFile) {
		self.audioFile = audioFile
		do {
			try AVAudioSession.sharedInstance().setPreferredSampleRate(8000)
		} catch let error {
			print("!! \(error)")
		}
		
		//		recordAndDownSampleAudioFromMicrophone()
		//		recordAudioFromMicrophone()
		
		// This doesn't work!!
		// Audio is "sloooowwwweeeed" down
		// This is because the audio isn't been down sampled
		//		recordAudioFromMicrophoneWithFileCache()
		//		recordAndDownSampleAudioFromMicrophoneWithFileCache()
//		recordAndDownSampleAudioFromMicrophoneWithFileCache2()
	}
//
//	func prepareAudioEngine() {
//		let engineInputNode = audioEngine.inputNode
//		let bus = 0
//		let engineInputNodeFormat = engineInputNode.outputFormat(forBus: bus)
//
////		audioEngine.attach(micMixerNode)
////		audioEngine.connect(engineInputNode, to: micMixerNode, format: engineInputNodeFormat)
//	}
//
//	//	func recordAudioFromMicrophone() {
//	//		prepareAudioEngine()
//	//		let engineInputNode = audioEngine.inputNode
//	//		let bus = 0
//	//		let engineInputNodeFormat = engineInputNode.outputFormat(forBus: bus)
//	//
//	//		//engineInputNode.installTap(onBus: bus, bufferSize: 1024 * 1024, format: engineInputNodeFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
//	//		micMixerNode.installTap(onBus: bus, bufferSize: 1024 * 1024, format: engineInputNodeFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
//	//			do {
//	//				let audioFile = try self.makeAudioFile()
//	//				try audioFile.1.write(from: buffer)
//	//
//	//				DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 5, execute: {
//	//					TransportQueue.shared.put(audioFile.0)
//	//				})
//	//			} catch let error {
//	//				print("!! \(error)")
//	//			}
//	//		}
//	//	}
//
//	// MARK: Take audio from microphone, down sample, send to encoder
//
//	func recordAndDownSampleAudioFromMicrophone() {
//		prepareAudioEngine()
//		let engineInputNode = audioEngine.inputNode
//		let bus = 0
//		let engineInputNodeFormat = engineInputNode.outputFormat(forBus: bus)
//
//		// This attempts to down sample the audio from the microphone
//		let mixer = AVAudioMixerNode()
//		audioEngine.attach(mixer)
//
//		// Not sure I've got these mixers set up right
//		let mixerOutputFormat = AVAudioFormat(standardFormatWithSampleRate: 8000, channels: 1)
//
////		audioEngine.connect(micMixerNode, to: mixer, format: engineInputNodeFormat)
//		audioEngine.connect(mixer, to: audioEngine.outputNode, format: mixerOutputFormat)
//
//		mixer.installTap(onBus: bus, bufferSize: 1024 * 4, format: mixerOutputFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
//			AudioEncoderQueue.shared.put(buffer)
//		}
//	}
//
//	// MARK: Take audio from the microphone, no downsample, send to encoder
//
//	func recordAudioFromMicrophone() {
//		prepareAudioEngine()
//		let engineInputNode = audioEngine.inputNode
//		let bus = 0
//		let engineInputNodeFormat = engineInputNode.outputFormat(forBus: bus)
//
//		engineInputNode.volume = 1.0
//
//		//engineInputNode.installTap(onBus: bus, bufferSize: 1024 * 1024, format: engineInputNodeFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
////		micMixerNode.installTap(onBus: bus, bufferSize: 1024 * 1024, format: engineInputNodeFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
////			AudioEncoderQueue.shared.put(buffer)
////		}
//	}
//
//	// MARK: Take audio from microphone, down sample, write to file, send file to transporter
//
//	var recording: AVAudioFile?
//
//	func recordAudioFromMicrophoneWithFileCache() {
//		prepareAudioEngine()
//		let engineInputNode = audioEngine.inputNode
//		let bus = 0
//		let engineInputNodeFormat = engineInputNode.outputFormat(forBus: bus)
//
//		engineInputNode.volume = 1.0
//
//		do {
//			let make = try makeAudioFile(named: "TestAudio")
//			print(">> Recording to \(make.0)")
//			recording = make.1
//		} catch let error {
//			print("!! Recording \(error)")
//		}
//
//		//engineInputNode.installTap(onBus: bus, bufferSize: 1024 * 1024, format: engineInputNodeFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
////		micMixerNode.installTap(onBus: bus, bufferSize: 1024 * 1024, format: engineInputNodeFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
////			AudioEncoderQueue.shared.put(buffer)
////			do {
////				if let recording = self.recording {
////					try recording.write(from: buffer)
////				}
////				let audioFile = try self.makeAudioFile()
////				try audioFile.1.write(from: buffer)
////
////				DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 5, execute: {
////					TransportQueue.shared.put(audioFile.0)
////				})
////			} catch let error {
////				print("!! \(error)")
////			}
////		}
//	}
//
//	func recordAndDownSampleAudioFromMicrophoneWithFileCache() {
//		let engineInputNode = audioEngine.inputNode
//		let bus = 0
//		let engineInputNodeFormat = engineInputNode.outputFormat(forBus: bus)
//
//		// This attempts to down sample the audio from the microphone
//		let mixer = AVAudioMixerNode()
//		audioEngine.attach(mixer)
//
//		let mixerOutputFormat = AVAudioFormat(standardFormatWithSampleRate: 8000, channels: 1)
//
//		audioEngine.connect(engineInputNode, to: mixer, format: engineInputNodeFormat)
//		audioEngine.connect(mixer, to: audioEngine.outputNode, format: mixerOutputFormat)
//
//		do {
//			let make = try makeAudioFile(named: "TestAudioDownSampled")
//			print(">> Recording to \(make.0)")
//			recording = make.1
//		} catch let error {
//			print("!! Recording \(error)")
//		}
//
//		mixer.installTap(onBus: bus, bufferSize: 1024 * 4, format: mixerOutputFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
//			do {
//				if let recording = self.recording {
//					try recording.write(from: buffer)
//				}
//				let audioFile = try self.makeAudioFile()
//				try audioFile.1.write(from: buffer)
//
//				//DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 5, execute: {
//				TransportQueue.shared.put(audioFile.0)
//				//})
//			} catch let error {
//				print("!! \(error)")
//			}
//		}
//	}
//
//	func recordAndDownSampleAudioFromMicrophoneWithFileCache2() {
//		let engineInputNode = audioEngine.inputNode
//		let bus = 0
//		let engineInputNodeFormat = engineInputNode.outputFormat(forBus: bus)
//
////		audioEngine.attach(micMixerNode)
////		audioEngine.connect(engineInputNode, to: micMixerNode, format: engineInputNodeFormat)
//
//		// This attempts to down sample the audio from the microphone
//		let downSampleMixer = AVAudioMixerNode()
//		audioEngine.attach(downSampleMixer)
//
//		let mixerOutputFormat = AVAudioFormat(standardFormatWithSampleRate: 8000, channels: 1)
//
//		// Input -> (volume) -> down sample -> (volume) -> Output
//
//		let inputVolume = AVAudioMixerNode()
//		inputVolume.volume = 32
//		audioEngine.attach(inputVolume)
//
//		let outputVolume = AVAudioMixerNode()
//		outputVolume.volume = 0
//		outputVolume.outputVolume = 0
//		audioEngine.attach(outputVolume)
//
//		audioEngine.connect(engineInputNode, to: inputVolume, format: engineInputNodeFormat)
//		audioEngine.connect(inputVolume, to: downSampleMixer, format: engineInputNodeFormat)
//		audioEngine.connect(downSampleMixer, to: outputVolume, format: mixerOutputFormat)
//		audioEngine.connect(outputVolume, to: audioEngine.outputNode, format: mixerOutputFormat)
//
//		do {
//			recording = try makePublicAudioFile(named: "TestAudioDownSampled")
//		} catch let error {
//			print("!! Recording \(error)")
//		}
//
//		downSampleMixer.installTap(onBus: bus, bufferSize: 1024 * 4, format: mixerOutputFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
//			do {
//				if let recording = self.recording {
//					print("...")
//					try recording.write(from: buffer)
//				}
//				let audioFile = try self.makeAudioFile()
//				try audioFile.1.write(from: buffer)
//
//				//DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 5, execute: {
//				TransportQueue.shared.put(audioFile.0)
//				//})
//			} catch let error {
//				print("!! \(error)")
//			}
//		}
//	}
	
	internal var audioEngine: AVAudioEngine?
	internal var downSampleMixerNode: AVAudioMixerNode?
	internal var inputVolumeMixerNode: AVAudioMixerNode?
	internal var outputVolumeMixerNode: AVAudioMixerNode?
	internal var audioTap: AudioTap?
	
	internal func setupNodeChain() {
		guard let audioEngine = audioEngine else { return } // Fatal error ?
		
		let engineInputNode = audioEngine.inputNode
		let bus = 0
		let engineInputNodeFormat = engineInputNode.outputFormat(forBus: bus)
		
		// This attempts to down sample the audio from the microphone
		let downSampleMixerNode = AVAudioMixerNode()
		let mixerOutputFormat = AVAudioFormat(standardFormatWithSampleRate: 8000, channels: 1)
		
		// Input -> (volume) -> down sample -> (volume) -> Output
		
		let inputVolumeMixerNode = AVAudioMixerNode()
		inputVolumeMixerNode.volume = 32
		
		let outputVolumeMixerNode = AVAudioMixerNode()
		outputVolumeMixerNode.volume = 0.0001
		outputVolumeMixerNode.outputVolume = 0.0001

		audioEngine.attach(inputVolumeMixerNode)
		audioEngine.attach(downSampleMixerNode)
		audioEngine.attach(outputVolumeMixerNode)
		
		self.downSampleMixerNode = downSampleMixerNode
		self.inputVolumeMixerNode = inputVolumeMixerNode
		self.outputVolumeMixerNode = outputVolumeMixerNode

		audioEngine.connect(engineInputNode, to: inputVolumeMixerNode, format: engineInputNodeFormat)
		audioEngine.connect(inputVolumeMixerNode, to: downSampleMixerNode, format: engineInputNodeFormat)
		audioEngine.connect(downSampleMixerNode, to: outputVolumeMixerNode, format: mixerOutputFormat)
		audioEngine.connect(outputVolumeMixerNode, to: audioEngine.outputNode, format: mixerOutputFormat)
		
		downSampleMixerNode.installTap(onBus: bus, bufferSize: 1024 * 16, format: mixerOutputFormat) { (buffer: AVAudioPCMBuffer, time: AVAudioTime) in
			guard let tap = self.audioTap else { return }
			print("drip...")
			tap.drip(buffer: buffer, time: time)
		}
	}
	
	func start() throws {
		stop()
		let audioEngine = AVAudioEngine()
		self.audioEngine = audioEngine
		setupNodeChain()
		audioTap = FileCacheAudioTap(audioFile: audioFile!)
//		audioTap = EncoderAudioTap()
		audioTap?.open()
		audioEngine.prepare()
		print("Start audio engine")
		try audioEngine.start()
	}
	
	func stop() {
		guard let audioEngine = audioEngine else { return }
		audioEngine.stop()
		audioTap?.close()
		audioTap = nil
		
		downSampleMixerNode?.removeTap(onBus: 0)
		detach(downSampleMixerNode)
		detach(inputVolumeMixerNode)
		detach(outputVolumeMixerNode)
	}
	
	internal func detach(_ node: AVAudioMixerNode?) {
		guard let audioEngine = audioEngine, let node = node else { return }
		audioEngine.detach(node)
	}
	
//
//	fileprivate func clearCache() {
//		do {
//			let url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
//			let contents = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: [.skipsSubdirectoryDescendants])
//			for child in contents {
//				let name = child.lastPathComponent
//				guard !(name.hasPrefix("Test") && name.hasSuffix(".aac")) && child.isFileURL else { continue }
//				print("Remove \(name)")
//				do {
//					try FileManager.default.removeItem(at: child)
//				} catch let error {
////					print("!! Could not remove")
////					print("!! \(child)")
////					print("!! \(error)")
//				}
//			}
//		} catch let error {
//			print("!! Clean failed: \(error)")
//		}
//	}
//
//	fileprivate func makePublicAudioFile(named: String) throws -> AVAudioFile {
//		var url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//		url.appendPathComponent("\(named).aac", isDirectory: false)
//
//		let settings: [String: Any] = [
//			AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),
//			AVSampleRateKey: NSNumber(value: 8000),
//			AVNumberOfChannelsKey: NSNumber(value: 1),
//			AVEncoderBitRatePerChannelKey: NSNumber(value: 16),
//			AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.min.rawValue)
//		]
//		let audioFile = try AVAudioFile(forWriting: url, settings: settings)
//		return audioFile
//	}
//
//	fileprivate func makeAudioFile(named: String = UUID().uuidString) throws -> (URL, AVAudioFile) {
//		var url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
//		url.appendPathComponent("\(named).aac", isDirectory: false)
//
//		let settings: [String: Any] = [
//			AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),
//			AVSampleRateKey: NSNumber(value: 8000),
//			AVNumberOfChannelsKey: NSNumber(value: 1),
//			AVEncoderBitRatePerChannelKey: NSNumber(value: 16),
//			AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.min.rawValue)
//		]
//		let audioFile = try AVAudioFile(forWriting: url, settings: settings)
//		return (url, audioFile)
//	}
	
}
