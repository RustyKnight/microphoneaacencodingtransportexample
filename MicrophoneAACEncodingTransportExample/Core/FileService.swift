//
//  FileService.swift
//  MicrophoneAACEncodingTransportExample
//
//  Created by Shane Whitehead on 1/10/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

class FileService {

	fileprivate var stopped: Bool = false
	
	var sourceURL: URL?
	var aacFile: AACFile?
	
	init(url: URL) {
		sourceURL = url
	}
	
	func start() {
		guard !stopped else { return }
		DispatchQueue.global(qos: .userInitiated).async {
			self.run()
		}
	}
	
	func stop() {
		stopped = true
		sourceURL = nil
		aacFile?.close()
		aacFile = nil
	}
	
	func run() {
		print("Let's get this party started")
		guard let url = sourceURL else {
			print("URL is invalid :(")
			stop()
			return
		}
		print("url = \(url)")
		print("url.path = \(url.path)")
		while !FileManager.default.fileExists(atPath: url.path) {
			print("Waiting for file to appear")
			Thread.sleep(forTimeInterval: 1.0)
		}
		do {
			print("Create AACFile...")
			aacFile = try AACFile(source: url)
			var frame: AACFile.ADTSFrame?
			repeat {
				print("Next frame...")
				frame = try aacFile?.nextFrame()
				if let frame = frame {
					print("Transport that sucker...")
					DataTransportQueue.shared.put(frame.data)
				}
			} while frame != nil
		} catch let error {
			stop()
			print("AAC file I/O error: \(error)")
		}
		print("FileService did stop :/")
	}
	
//		var nextFile: AudioSnippet?
//		var isFirst = true
//		repeat {
//			nextFile = FileQueue.shared.take()
//			if let file = nextFile {
//				if isFirst {
//					print("Wait")
//					Thread.sleep(forTimeInterval: 5.0)
//					isFirst = false
//					print("Make it rain")
//				}
//				do {
//					var data = try Data(contentsOf: file.url)
//
//					while let nextFrame = nextFrame(buffer: &data) {
//						DataTransportQueue.shared.put(nextFrame)
//					}
//				} catch let error {
//					print("!! Failed to read audio file: \(error)")
//				}
//
//				// De-reference to close the file :/
//				file.audioFile = nil
////				do {
////					try FileManager.default.removeItem(at: file)
////				} catch let error {
////					print("""
////						!! Could not remove
////						\(file)
////						\(error)
////						""")
////				}
//			}
//		} while nextFile != nil
//	}
//
//	fileprivate func nextFrame(buffer: inout Data) -> Data? {
//		var size: Int = 0
//
//		guard !buffer.isEmpty else { return nil }
//		repeat {
//			if buffer.count < 7 { return nil }
//			if buffer[0] == 0xff && (buffer[1] & 0xf0) == 0xf0 {
//				size |= (Int(buffer[3]) & 0x03) << 11
//				size |= (Int(buffer[4])) << 3
//				size |= (Int(buffer[5]) & 0xe0) >> 5
//				break
//			}
//			//			log(debug: "Skip...")
//			buffer = buffer.advanced(by: 1)
//		} while true
//
//		//		log(debug: "size = \(size); remaing = \(remainingBuffer.count)")
//
//		size = min(size, buffer.count)
//
//		let nextFrame = buffer[0..<size]
//		buffer = buffer.advanced(by: size - 1)
//		return nextFrame
//	}

}
