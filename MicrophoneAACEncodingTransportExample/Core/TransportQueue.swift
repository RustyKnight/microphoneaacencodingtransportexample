
//
//  EncodeQueue.swift
//  AudioTest
//
//  Created by Shane Whitehead on 19/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import AVFoundation

// These are the data items which need to be transported
//class TransportQueue: BlockingQueue<Data> {
//	static let shared: TransportQueue = TransportQueue()
//}
class TransportQueue: BlockingQueue<URL> {
	static let shared: TransportQueue = TransportQueue()
}

class DataTransportQueue: BlockingQueue<Data> {
	static let shared: DataTransportQueue = DataTransportQueue()
}
