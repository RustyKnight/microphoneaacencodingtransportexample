//
//  BlockingQueue.swift
//  AudioTest
//
//  Created by Shane Whitehead on 19/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

// The blocking queue is pretty basic as is just a FIFO queue based
// on a producer/consumer paradigm, where the caller will block
// until a new element becomes avaliable
class BlockingQueue<Value> {
	let queueLock: NSObject = NSObject()
	let queueSemaphore: DispatchSemaphore = DispatchSemaphore(value: 1)
	
	// Basically we know what's going on here
	var items: [Value] = []
	
	fileprivate var stopped: Bool = false
	
	func start() {
		stop()
		lock(on: queueLock) {
			items.removeAll()
			stopped = false
		}
	}
	
	func stop() {
		guard !stopped else { return }
		
		lock(on: queueLock) {
			stopped = true
		}
		lock(on: queueLock) {
			items.removeAll()
			queueSemaphore.signal()
		}
	}
	
	func put(_ value: Value) {
		lock(on: queueLock) {
			guard !stopped else {
				return
			}
			items.append(value)
			queueSemaphore.signal()
		}
	}
	
	func take() -> Value? {
		var result: Value? = nil
		var acceptable = false
		repeat {
			acceptable = false
			
			result = lock(on: queueLock) {
				guard !items.isEmpty else { return nil }
				return items.removeFirst()
			}
			if result == nil {
				if stopped {
					acceptable = true
				} else {
					_ = queueSemaphore.wait(timeout: DispatchTime.now() + 3.0)
				}
			} else {
				acceptable = true
			}
		} while !acceptable
		return result
	}
}
