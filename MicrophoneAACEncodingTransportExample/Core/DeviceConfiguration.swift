//
//  DeviceConfiguration.swift
//  MicrophoneAACEncodingTransportExample
//
//  Created by Shane Whitehead on 17/9/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

struct DeviceConfiguration: Codable {
	let ipAddress: String
	let port: UInt16
}
