//
//  TransportService.swift
//  AudioTest
//
//  Created by Shane Whitehead on 19/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import SwiftSocket

class TransportService {
	
	fileprivate var stopped: Bool = false
	
	var socket: TCPClient?
	let config: DeviceConfiguration
	
	init(config: DeviceConfiguration) {
		self.config = config
	}
	
	func start() throws {
		guard !stopped else { return }
		stopped = false
		socket = TCPClient(address: config.ipAddress, port: Int32(config.port))
		switch socket!.connect(timeout: 30) {
		case .success:
			DispatchQueue.global(qos: .userInitiated).async {
				self.transportData()
//				self.transportFile()
//				self.transportFrame()
			}
		case .failure(let error):
			socket = nil
			throw error
		}
	}
	
	func stop() {
		stopped = true
	}
	
	// MARK: Send by data

	func transportData() {
		print(">> Transport data")
		guard let socket = socket else { return }

		let headerLength = MemoryLayout<SWANN_AUDIO_HEADER_ST>.size
		print("Header length = \(headerLength)")
		var header = SWANN_AUDIO_HEADER_ST()
		header.start_code = (Int8("S".utf8.first!), Int8("W".utf8.first!), Int8("S".utf8.first!), Int8("W".utf8.first!))
		header.header_length = Int32(headerLength)
		header.encoder_type = 2
		header.sample_rate = 8000

		var count = 0
		
		defer {
			self.socket?.close()
			self.socket = nil
		}
		
		repeat {
			guard let data = DataTransportQueue.shared.take() else { return }
			guard data.count > 0 else {
				print("Empty packet")
				continue
			}

			header.packet_sn = Int32(count)
			header.data_length = Int32(data.count)

			var headerData = Data(bytes: &header, count: headerLength)
			headerData.append(data)
			print("""
				---------------------
				          Sequence: \(count)
				 Audio packet size: \(data.count)
				Header data_length: \(header.data_length)
				       Header size: \(headerData.count)
				""")

			// Send the header
			let result = socket.send(data: headerData)
			switch result {
			case .success: break
			case .failure(let error):
				print("!! \(error)")
				return
			}
			count += 1

			Thread.sleep(forTimeInterval: 0.05)
		} while !stopped
	}

	
	func transportFrame() {
		print(">> Transport Frame")
		guard let socket = socket else { return }

		let headerLength = MemoryLayout<SWANN_AUDIO_HEADER_ST>.size
		print("Header length = \(headerLength)")
		var header = SWANN_AUDIO_HEADER_ST()
		header.start_code = (Int8("S".utf8.first!), Int8("W".utf8.first!), Int8("S".utf8.first!), Int8("W".utf8.first!))
		header.header_length = Int32(headerLength)
		header.encoder_type = 2
		header.sample_rate = 8000

		var count = 0
		
		defer {
			self.socket?.close()
			self.socket = nil
		}
		
		repeat {
			guard let data = DataTransportQueue.shared.take() else { return }
			guard data.count > 0 else {
				print("Empty packet")
				continue
			}
			
			let adtsFrame = ADST.frame(forAudioPacket: data)

			header.packet_sn = Int32(count)
			header.data_length = Int32(adtsFrame.count)

			var headerData = Data(bytes: &header, count: headerLength)
			headerData.append(adtsFrame)
			print("""
				---------------------
				          Sequence: \(count)
				 Audio packet size: \(data.count)
				        ADTS Frame: \(adtsFrame.count)
				Header data_length: \(header.data_length)
				       Header size: \(headerData.count)
				""")

			// Send the header
			let result = socket.send(data: headerData)
			switch result {
			case .success: break
			case .failure(let error):
				print("!! \(error)")
				return
			}
			count += 1

			Thread.sleep(forTimeInterval: 0.05)
		} while !stopped
	}

}
