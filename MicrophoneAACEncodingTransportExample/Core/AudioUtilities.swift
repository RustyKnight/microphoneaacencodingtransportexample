//
//  AudioUtilities.swift
//  AudioTest
//
//  Created by Shane Whitehead on 16/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import AVFoundation

// https://stackoverflow.com/questions/51360127/decode-aac-to-pcm-format-using-avaudioconverter-swift

// Basically, this is a implementation taken from the above stack overflow
// answer which attempts to encode a AVAudioBuffer using AAC
// The target requirement must have the audio encoded in AAC, 8khz, mono
class AudioUtilities {
	
	static func PCMFormat() -> AVAudioFormat? {
		return AVAudioFormat(commonFormat: .pcmFormatFloat32, sampleRate: 8000, channels: 1, interleaved: false)
	}
	
	static func AACFormat() -> AVAudioFormat? {

		var outDesc = AudioStreamBasicDescription(
			mSampleRate: 8000,
			mFormatID: kAudioFormatMPEG4AAC,
			mFormatFlags: 0,
			mBytesPerPacket: 0,
			mFramesPerPacket: 0,
			mBytesPerFrame: 0,
			mChannelsPerFrame: 1,
			mBitsPerChannel: 0,
			mReserved: 0)
		
		let outFormat = AVAudioFormat(streamDescription: &outDesc)
		return outFormat
	}
	
	static var lpcmToAACConverter: AVAudioConverter! = nil
	
	static func convertToAAC(from buffer: AVAudioBuffer) throws -> AVAudioCompressedBuffer? {
		
		let outputFormat = AACFormat()
		//init converter once
		if lpcmToAACConverter == nil {
			let inputFormat = buffer.format

			lpcmToAACConverter = AVAudioConverter(from: inputFormat, to: outputFormat!)
			lpcmToAACConverter.bitRate = 8000
		}
		let outBuffer = AVAudioCompressedBuffer(format: outputFormat!,
																						packetCapacity: 8,
																						maximumPacketSize: lpcmToAACConverter.maximumOutputPacketSize)
//																						maximumPacketSize: 768)
		
		try self.convert(withConverter: lpcmToAACConverter,
										 from: buffer,
										 to: outBuffer)

		return outBuffer
	}
	
	static var aacToLPCMConverter: AVAudioConverter! = nil

	static func convertToPCM(from buffer: AVAudioBuffer) throws -> AVAudioPCMBuffer? {

		let outputFormat = PCMFormat()
		guard let outBuffer = AVAudioPCMBuffer(pcmFormat: outputFormat!, frameCapacity: 4410) else {
			return nil
		}

		//init converter once
		if aacToLPCMConverter == nil {
			let inputFormat = buffer.format

			aacToLPCMConverter = AVAudioConverter(from: inputFormat, to: outputFormat!)
		}

		try self.convert(withConverter: aacToLPCMConverter, from: buffer, to: outBuffer)

		return outBuffer
	}
//
//	static func convertToAAC(from data: Data, packetDescriptions: [AudioStreamPacketDescription]) -> AVAudioCompressedBuffer? {
//
//		let nsData = NSData(data: data)
//		let inputFormat = AACFormat()
//		let maximumPacketSize = packetDescriptions.map { $0.mDataByteSize }.max()!
//		let buffer = AVAudioCompressedBuffer(format: inputFormat!, packetCapacity: AVAudioPacketCount(packetDescriptions.count), maximumPacketSize: Int(maximumPacketSize))
//		buffer.byteLength = UInt32(data.count)
//		buffer.packetCount = AVAudioPacketCount(packetDescriptions.count)
//
//		buffer.data.copyMemory(from: nsData.bytes, byteCount: nsData.length)
//		buffer.packetDescriptions!.pointee.mDataByteSize = UInt32(data.count)
//		buffer.packetDescriptions!.initialize(from: packetDescriptions, count: packetDescriptions.count)
//
//		return buffer
//	}
//
	
	private static func convert(withConverter: AVAudioConverter, from sourceBuffer: AVAudioBuffer, to destinationBuffer: AVAudioBuffer) throws {
		// input each buffer only once
		var newBufferAvailable = true

		let inputBlock : AVAudioConverterInputBlock = {
			inNumPackets, outStatus in
			if newBufferAvailable {
				outStatus.pointee = .haveData
				newBufferAvailable = false
				return sourceBuffer
			} else {
				outStatus.pointee = .noDataNow
				return nil
			}
		}

		var outError: NSError? = nil
		let status = withConverter.convert(to: destinationBuffer, error: &outError, withInputFrom: inputBlock)

		switch status {
		case .haveData: break //print("Have data :)")
		case .inputRanDry: print("Input ran dry")
		case .endOfStream: print("?? End of stream")
		case .error: print("!! !! Error")
		@unknown default:
			print("Unknown status")
		}

		guard let error = outError else {
			return
		}
		print("!! \(error.localizedDescription)")
		throw error
	}
}
