//
//  SwannAudioHeader.h
//  AudioTest
//
//  Created by Shane Whitehead on 13/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

#ifndef SwannAudioHeader_h
#define SwannAudioHeader_h

typedef struct{
	char start_code[4]; //SWSW
	int header_length; //24
	int encoder_type; //2, i.e. AAC
	int sample_rate; //8000 or 16000 depending on device
	int packet_sn;
	int data_length; // size of each AAC ADTS unit
}SWANN_AUDIO_HEADER_ST;

#endif /* SwannAudioHeader_h */
