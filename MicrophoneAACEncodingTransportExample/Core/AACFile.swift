//
//  ACCFile.swift
//  MicrophoneAACEncodingTransportExample
//
//  Created by Shane Whitehead on 23/9/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

class AACFile {
	
	class ADTSFrame {
		let size: Int
		let data: Data
		
		init(size: Int, data: Data) {
			self.size = size
			self.data = data
		}
	}

	var handle: FileHandle?
	fileprivate var isClosed = false
	
	init(source: URL) throws {
		handle = try FileHandle(forReadingFrom: source)
	}
	
	deinit {
		close()
	}
	
	func close() {
		isClosed = true
		handle?.closeFile()
		handle = nil
	}
	
	func nextFrame() throws -> ADTSFrame? {
		guard let handle = handle else { return nil }
		
		var size: Int = 0
		var readyToRead = false
		repeat {
			let startAt = handle.offsetInFile
			let header = handle.readData(ofLength: 7)
			guard header.count == 7 else {
				guard !isClosed else { return nil }
				// Try and wait for some data
				print("Wait for header to become avaliable")
				Thread.sleep(forTimeInterval: 0.1)
				guard !isClosed else { return nil }
				handle.seek(toFileOffset: startAt)
				continue
			}
			// Check for magic header
			guard header[0] == 0xff && (header[1] & 0xf0) == 0xf0 else {
				// Advance by one byte
				handle.seek(toFileOffset: startAt + 1)
				continue
			}
			
			size |= (Int(header[3]) & 0x03) << 11
			size |= (Int(header[4])) << 3
			size |= (Int(header[5]) & 0xe0) >> 5

			readyToRead = true
		} while !readyToRead
		
		// This will need to wait to there are enough bytes to read
		
		// Header reports size as header + frame
		print("Read frame @ \(size)")
		let startAt = handle.offsetInFile - 7
		var frameData: Data!
		repeat {
			guard !isClosed else { return nil }
			handle.seek(toFileOffset: startAt)
			frameData = handle.readData(ofLength: size)
			// We might not have enough data to read yet,
			// so we might need to wait for data to
			// become avaliable
			guard frameData.count == size else {
				guard !isClosed else { return nil }
				print("Wait for frame to become avaliable")
				Thread.sleep(forTimeInterval: 0.1)
				continue
			}
		} while frameData.count < size
		
		return ADTSFrame(size: size, data: frameData)
	}

}
