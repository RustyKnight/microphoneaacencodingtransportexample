//
//  AudioEncoderService.swift
//  AudioTest
//
//  Created by Shane Whitehead on 19/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import AVFoundation

class AudioEncoderService {
	
	fileprivate var stopped: Bool = false
	
	fileprivate lazy var aacFormat: AVAudioFormat? = {
		
		var outDesc = AudioStreamBasicDescription(
			mSampleRate: 8000,
			mFormatID: kAudioFormatMPEG4AAC,
			mFormatFlags: 0,
			mBytesPerPacket: 0,
			mFramesPerPacket: 0,
			mBytesPerFrame: 0,
			mChannelsPerFrame: 1,
			mBitsPerChannel: 0,
			mReserved: 0)
		
		let outFormat = AVAudioFormat(streamDescription: &outDesc)
		return outFormat
	}()
	
	var lpcmToAACConverter: AVAudioConverter! = nil
	
	init() throws {
	}
	
	func start() {
		DispatchQueue.global(qos: .userInitiated).async {
			self.encodeAudio()
		}
	}
	
	func stop() {
		stopped = true
	}
	
	func encodeAudio() {
		
		// Grab the desired output format
		let outputFormat = aacFormat
		
		// Get the first audio packet
		guard let initialBuffer = AudioEncoderQueue.shared.take() else {
			print("!! Failed to prime initial buffer")
			return
		}
		
		// Initialise the convert, we need the input format from the first packet
		if lpcmToAACConverter == nil {
			let inputFormat = initialBuffer.format
			
			lpcmToAACConverter = AVAudioConverter(from: inputFormat, to: outputFormat!)
			lpcmToAACConverter.bitRate = 8000
		}
		// Create the output buffer
		let outBuffer = AVAudioCompressedBuffer(format: outputFormat!,
																						packetCapacity: 3,
																						maximumPacketSize: lpcmToAACConverter.maximumOutputPacketSize)
//																								maximumPacketSize: 768)
		
		// Set the first buffer, so we can set it to till in our conversion block
		var firstBuffer: AVAudioPCMBuffer? = initialBuffer
		
		repeat {
			
			// Setup our conversion block, this will keep seeding audio packets
			// from the qeueue
			let inputBlock : AVAudioConverterInputBlock = { inNumPackets, outStatus in
				// Return the initial buffer its avaliable, otherwise get the next
				// packet from the queueu
				if let buffer = firstBuffer {
					outStatus.pointee = .haveData
					firstBuffer = nil
					return buffer
				} else if let buffer = AudioEncoderQueue.shared.take() {
					outStatus.pointee = .haveData
					return buffer
				}
				
				// We "assume" we've reached the end of the stream otherwise...
				// Safe place fall back
				outStatus.pointee = .endOfStream
				return nil
			}
			
			// Convert the audio, writing it to the output buffer
			var outError: NSError? = nil
			let status = lpcmToAACConverter.convert(to: outBuffer, error: &outError, withInputFrom: inputBlock)
			// Check the state
			switch status {
			case .inputRanDry:
				print("Input ran dry")
				fallthrough
			case .haveData:
				let data = Data(bytes: outBuffer.data, count: Int(outBuffer.byteLength))
				//print("Encoded audio data = \(data.count) bytes")
				DataTransportQueue.shared.put(data)
			case .endOfStream:
				print("?? End of stream")
				stop()
			case .error: print("!! !! Error")
			@unknown default:
				print("Unknown status")
			}
			
			if let error = outError {
				print("!! \(error.localizedDescription)")
				stop()
			}
			
		} while !stopped
		print("AudioEncoderService stopped")
	}
	
}
