//
//  ADTS.swift
//  MicrophoneAACEncodingTransportExample
//
//  Created by Shane Whitehead on 20/9/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

// https://wiki.multimedia.cx/index.php/ADTS
// https://stackoverflow.com/questions/14934305/pcm-to-aac-conversion-using-mediacodec/17357008#17357008
// https://stackoverflow.com/questions/18862715/how-to-generate-the-aac-adts-elementary-stream-with-android-mediacodec/18970406#18970406

struct ADST {
	
//	adtsHeader[0] = (byte) 0xFF; // Sync Word
//	adtsHeader[1] = (byte) 0xF1; // MPEG-4, Layer (0), No CRC
//	adtsHeader[2] = (byte) ((MediaCodecInfo.CodecProfileLevel.AACObjectLC - 1) << 6);
//	adtsHeader[2] |= (((byte) SAMPLE_RATE_INDEX) << 2);
//	adtsHeader[2] |= (((byte) CHANNELS) >> 2);
//	adtsHeader[3] = (byte) (((CHANNELS & 3) << 6) | ((frameLength >> 11) & 0x03));
//	adtsHeader[4] = (byte) ((frameLength >> 3) & 0xFF);
//	adtsHeader[5] = (byte) (((frameLength & 0x07) << 5) | 0x1f);
//	adtsHeader[6] = (byte) 0xFC;
	
	static func frame(forAudioPacket audioPacket: Data) -> Data {
		let audioSize = audioPacket.count // in bytes
		let frameSize = audioSize + 7 // 7 bytes for header
		
		var header = Data(capacity: frameSize)
		
		// https://wiki.multimedia.cx/index.php/MPEG-4_Audio#Audio_Object_Types
		let audioObjectType: UInt8 = 2 // AAC LC
		// https://wiki.multimedia.cx/index.php/MPEG-4_Audio#Sampling_Frequencies
		let samplingFrequencies: UInt8 = 11 //??
		let channels: UInt8 = 1 // One channel
		
		let byte2 = UInt8((audioObjectType - 1) << 6) + UInt8(samplingFrequencies << 2) + UInt8(channels >> 2)
		//let byte3 = UInt8((channels & 3) << 6) + UInt8(frameSize >> 11)
		let byte3 = UInt8((channels & 3) << 6) + UInt8((frameSize >> 11) & 0x03)
//		let byte4 = UInt8((frameSize & 0x7FF) >> 3)
		let byte4 = UInt8((frameSize >> 3) & 0xff)
		let byte5 = UInt8(((frameSize & 7) << 5) + 0x1f)
		let byte6 = UInt8(0xfc)
		
		header.append(0xff)
		header.append(0xf9)
		header.append(byte2)
		header.append(byte3)
		header.append(byte4)
		header.append(byte5)
		header.append(byte6)
		
		header.append(audioPacket)
		
		return header
	}
}
