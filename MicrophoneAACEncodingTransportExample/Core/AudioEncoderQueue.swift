//
//  EncodeQueue.swift
//  AudioTest
//
//  Created by Shane Whitehead on 19/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation
import AVFoundation

// These are the data items which need to be encoded
class AudioEncoderQueue: BlockingQueue<AVAudioPCMBuffer> {
	static let shared: AudioEncoderQueue = AudioEncoderQueue()
}
